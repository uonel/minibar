package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

type ContentType int

const (
	WwwForm ContentType = iota
	Json
	UnsupportedType
)

type IndieAuthRes struct {
	Me       string `json:"me"`
	ClientId string `json:"client_id"`
	Scope    string `json:"scope"`
	Issue    int    `json:"issued_at"`
	Nonce    int    `json:"nonce"`
}

// TODO: Scrape token endpoint from url and save it. Then use saved one and only rescrape if saved one doesn't work.
func checkAccess(token string, action string) (bool, error) {
	if token == "" {
		return false, errors.New("token string is empty")
	}
	// form the request to check the token
	client := http.DefaultClient
	req, err := http.NewRequest("GET", IndieAuthTokenUrl, nil)
	if err != nil {
		return false, errors.New("error making the request for checking token access")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	// send the request
	res, err := client.Do(req)
	if err != nil {
		return false, errors.New("error sending the request for checking token access")
	}

	// parse the response
	indieAuthRes := &IndieAuthRes{}
	err = json.NewDecoder(res.Body).Decode(&indieAuthRes)
	res.Body.Close()
	if err != nil {
		return false, errors.New("the token endpoint didn't return valid json")
	}
	// verify results of the response
	if CleanUrl(indieAuthRes.Me) != SiteUrl {
		return false, errors.New("me does not match")
	}
	scopes := strings.Fields(indieAuthRes.Scope)
	scopeReached := false
	for _, scope := range scopes {
		if scope == action {
			scopeReached = true
			break
		}
	}
	if !scopeReached {
		return false, errors.New("you only have permission to "+indieAuthRes.Scope+" not "+action)
	}
	return true, nil
}

func GetContentType(contentType string) (ContentType, error) {
	if contentType != "" {
		if strings.Contains(contentType, "application/x-www-form-urlencoded") {
			return WwwForm, nil
		}
		if strings.Contains(contentType, "application/json") {
			return Json, nil
		}
		if strings.Contains(contentType, "multipart/form-data") {
			return UnsupportedType, errors.New("multipart/form-data is not supported, use media endpoint instead")
		}
		return UnsupportedType, errors.New("content-type " + contentType + " is not supported, use application/x-www-form-urlencoded or application/json")
	}
	return UnsupportedType, errors.New("content-type is not provided in the request")
}
