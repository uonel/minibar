import sys
import os
import re
import json
import random
import datetime
import subprocess
import pickle
import requests
from slugify import UniqueSlugify


micropubobject = json.loads(sys.stdin.read())
directory = "content/p"

slugify = UniqueSlugify(
    max_length=16,
    to_lower=True,
    unique_check=lambda t, _: not os.path.isfile(directory + "/" + t + ".md"),
)


def cleanhtml(raw_html):
    cleanr = re.compile("<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});")
    cleantext = re.sub(cleanr, "", raw_html)
    return cleantext


def getslug(mpobject):
    if "mp-slug" in properties:
        # TODO: first if it already exists
        return slugify(properties["mp-slug"][0])
    elif "title" in properties:
        return slugify(properties["name"][0])
    elif "content" in properties:
        if isinstance(properties["content"][0], dict):
            if "html" in properties["content"][0]:
                return slugify(cleanhtml(properties["content"][0]["html"]))
            # TODO: also support markdown
        elif isinstance(properties["content"][0], str):
            return slugify(properties["content"][0])
    else:
        # generate something random
        return slugify(
            "".join([random.choice("abcdefghijklmnopqrstuvw") for x in range(16)])
        )


def getshort():
    shorts = []
    if os.path.exists(".shortlinks.pickle"):
        with open(".shortlinks.pickle", "rb") as f:
            shorts = pickle.load(f)
    short = "".join(
        [
            random.choice(
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
            )
            for x in range(3)
        ]
    )
    while (
        short not in shorts
        and requests.get("https://úl.de/p/" + short).status_code != 404
    ):
        short = "".join(
            [
                random.choice(
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
                )
                for x in range(3)
            ]
        )  # 1 124 possibilities
        shorts.append(short)
    with open(".shortlinks.pickle", "wb") as f:
        pickle.dump(shorts, f)
    return short


def readurl(url):
    path = micropubobject["url"][0].replace("https://ulpaulpa.de/", "")
    if path.endswith("/"):
        path = path[:-1]
    filename = path + ".md"
    filejson = subprocess.run(
        f"./hugocat.sh toJSON {filename}", shell=True, capture_output=True
    ).stdout.decode()
    regmatch = re.fullmatch("({(?:.?\n?)+})((?:.?\n?)+)", filejson)
    frontmatter = regmatch.group(1)
    content = regmatch.group(2).strip()
    return json.loads(frontmatter), content, "content/" + filename


with open("getjson" if micropubobject["method"][0] == "GET" else "postjson", "w") as f:
    f.write(json.dumps(micropubobject, indent=4))
    f.write(micropubobject["method"][0])

if micropubobject["method"][0] == "POST":
    if micropubobject["action"][0] == "create":
        properties = micropubobject["properties"]
        if micropubobject["type"][0] == "h-entry":
            if "name" in properties:
                properties["title"] = properties["name"][0]
                del properties["name"]
            if "published" in properties:
                properties["date"] = properties["published"][0]
                del properties["published"]
            else:
                properties["date"] = (
                    datetime.datetime.now()
                    .astimezone()
                    .replace(microsecond=0)
                    .isoformat()
                )
            if "category" in properties:
                properties["t"] = properties["category"]
                del properties["category"]
            if "photo" in properties:
                newphotos = list()
                for photo in properties["photo"]:
                    if isinstance(photo, str):
                        newphotos.append({"value": photo})
                    else:
                        newphotos.append(photo)
                properties["photo"] = newphotos
            if isinstance(properties["content"][0], dict):
                content = properties["content"][0]["html"]
            elif isinstance(properties["content"][0], str):
                content = properties["content"][0]
            properties["short"] = getshort()
            filename = directory + "/" + getslug(micropubobject) + ".md"
            if "mp-slug" in properties:
                del properties["mp-slug"]
            del properties["content"]
            with open(filename, "w") as f:
                f.write(json.dumps(properties, sort_keys=True, indent=4) + "\n\n")
                f.write(content)
            print(
                "https://ulpaulpa.de/"
                + filename[:-3].replace("content/", "").lower()
                + "/"
            )
    elif micropubobject["action"][0] == "delete":
        frontmatter, content, filename = readurl(micropubobject["url"][0])
        frontmatter["draft"] = True
        with open(filename, "w") as f:
            f.write(json.dumps(frontmatter, indent=4) + "\n\n" + content)
        path = micropubobject["url"][0].replace("https://ulpaulpa.de/", "")
        path = path[:-1] if path.endswith("/") else path
        os.remove("/home/ulpa/html/" + path + "/index.html")
        print(micropubobject["url"][0])
    elif micropubobject["action"][0] == "undelete":
        frontmatter, content, filename = readurl(micropubobject["url"][0])
        frontmatter["draft"] = False
        with open(filename, "w") as f:
            f.write(json.dumps(frontmatter, indent=4) + "\n\n" + content)
    elif micropubobject["action"][0] == "update":
        properties, content, filename = readurl(micropubobject["url"][0])
        if "title" in properties:
            properties["name"] = [properties.pop("title")]
        if "date" in properties:
            properties["published"] = [properties.pop("date")]
        if "t" in properties:
            properties["category"] = properties.pop("t")
        properties["content"] = [content]

        if "replace" in micropubobject:
            for k, v in micropubobject["replace"].items():
                properties[k] = v
        if "add" in micropubobject:
            for k, v in micropubobject["add"].items():
                if k in properties:
                    properties[k] += v
                else:
                    properties[k] = v
        if "delete" in micropubobject:
            if isinstance(micropubobject["delete"], dict):
                for k, v in micropubobject["delete"].items():
                    if k in properties:
                        for x in v:
                            properties[k].remove(x)
            elif isinstance(micropubobject["delete"], list):
                for k in micropubobject["delete"]:
                    if k in properties:
                        del properties[k]

        if "name" in properties:
            properties["title"] = properties["name"][0]
        if "published" in properties:
            properties["date"] = properties["published"][0]
            del properties["published"]
        else:
            properties["date"] = (
                datetime.datetime.now().astimezone().replace(microsecond=0).isoformat()
            )
        if "category" in properties:
            properties["t"] = properties["category"]
        content = properties.pop("content")[0]
        with open(filename, "w") as f:
            f.write(json.dumps(properties, indent=4) + "\n\n" + content)
        print(micropubobject["url"][0])


elif micropubobject["method"][0] == "GET":
    if micropubobject["q"][0] == "config":
        print(json.dumps({"media-endpoint": "https://ulpaulpa.de/media"}))
    elif micropubobject["q"][0] == "syndicate-to":
        print(json.dumps({"syndicate-to": []}))
    elif micropubobject["q"][0] == "source":
        properties, content, _ = readurl(micropubobject["url"][0])
        if "title" in properties:
            properties["name"] = [properties.pop("title")]
        if "date" in properties:
            properties["published"] = [properties.pop("date")]
        if "t" in properties:
            properties["category"] = properties.pop("t")
        if "short" in properties:
            del properties["short"]
        properties["content"] = [content]
        print(json.dumps({"properties": properties, "type": ["h-entry"]}))
