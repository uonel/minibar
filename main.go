package main

import (
	"log"
	"net/http"
	"github.com/spf13/cobra"
	"fmt"
	"os"
	"strings"
)

var (
	IndieAuthTokenUrl string
	SiteUrl string
	port string
	Script []string
)

var rootCmd = &cobra.Command{
	Use: "minibar site-url",
	Short: "A server implementation for Micropub",
	Long:`Server implementaion for Micropub, that handles json and wwform,
	checks access_token and permission and passes a json object to a
	customizable script.`,
	Args: cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		SiteUrl = CleanUrl(args[0])
		Script = args[1:]
		log.Println("Starting micropub server...")
		http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
			status, err := HandleMicroPub(w, r)
			log.Println(status, err)
			if err != nil {
				w.WriteHeader(status)
				w.Write([]byte(err.Error()))
			}
		})
		log.Println("Site Url: "+SiteUrl)
		log.Println("Listening on Port "+port)
		log.Println("Script: "+strings.Join(Script, " "))
		log.Fatal(http.ListenAndServe(":"+port, nil))
	},
}

func main() {
	rootCmd.Flags().StringVarP(&port, "port", "p", "5555", "The port micropub-server will bind to")
	rootCmd.Flags().StringVarP(&IndieAuthTokenUrl, "token-endpoint", "t", "https://tokens.indieauth.com/token", "The Tokenendpoint you are using.")
	if err := rootCmd.Execute(); err != nil {
    fmt.Fprintln(os.Stderr, err)
    os.Exit(1)
	}
}
